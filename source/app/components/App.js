const React = require('react');

const Days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
const monthName = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
const daysInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

const today = new Date();
// todayMonth == 0 => January
const todayMonth = today.getUTCMonth();
const todayDate = today.getUTCDate();
const todayDay = today.getUTCDay();
const todayYear = today.getUTCFullYear();

function request(method, url, data, success, fail) {
  const http = new XMLHttpRequest();
  http.open(method, url, true);

  http.setRequestHeader('Content-Type', 'application/json');

  http.onreadystatechange = function() {
    if (http.readyState == 4 && http.status == 200) {
      success && success(http);
    } else {
      fail && fail(http);
    }
  };

  http.send(JSON.stringify(data));
}


class Cell extends React.Component {
  render() {
    const { propYear, month, notes, onClick } = this.props;
    const thisMonth = monthName[month];
    const previousMonth = (month === 0) ? monthName[11] : monthName[month - 1] ;
    const nextMonth = (month === 11) ? monthName[0] : monthName[month+1];
    let numberOfDays = 0;
    if (month === 1) {
      if((propYear % 4 === 0 && propYear % 100 != 0) || propYear % 400 === 0 ) {
        numberOfDays = 29;
      }
    } else {
      numberOfDays = daysInMonth[month];
    }
    const firstDay = new Date(propYear, month, 1);
    //startOfDay = 0 => Monday
    const startOfDay = firstDay.getDay();
    const year = propYear;
    const numberOfPreviousMonthDisplayed = startOfDay;
    const numberOfNextMonthDisplayed = 42 - numberOfDays - numberOfPreviousMonthDisplayed;
    const numberOfDaysPreviousMonth = (month === 0) ? daysInMonth[11] : daysInMonth[month - 1];

    let dates = [[]];
    let rowIndex = 0;

    let index = 0;
    //Previous month's last few days that's displayed on the current month
    for (index; index < numberOfPreviousMonthDisplayed; index++) {
      dates[rowIndex].push({
        date: numberOfDaysPreviousMonth - numberOfPreviousMonthDisplayed + index + 1,
        month: previousMonth
      });
    }
    
    //Current month's dates
    for (index; index < numberOfDays + numberOfPreviousMonthDisplayed; index++) {
      if (index % 7 == 0) {
        rowIndex ++;
        dates[rowIndex] = [];
      }
      dates[rowIndex].push({
        date: index - numberOfPreviousMonthDisplayed + 1,
        month: thisMonth
      });
    }

    let nextMonthDate = 1;
    //Next month's first few days
    for (index; index < 42; index++) {
      if (index % 7 == 0) {
        rowIndex ++;
        dates[rowIndex] = [];
      }
      dates[rowIndex].push({
        date: nextMonthDate,
        month: nextMonth
      });
      nextMonthDate ++;
    }

    const { selectedDate, selectedMonth } = this.props;
    
    let allDates = dates.map(function (rowOfCells) {
      let row = rowOfCells.map(function (cell) {
        let isItThisMonth = [ thisMonth ].indexOf(cell.month) !== -1;
        let className = `Cell ${isItThisMonth ? '' : 'Cell_Gray'} ${(selectedDate === cell.date && selectedMonth === cell.month) ? 'Cell--selected' : ''}`;
        const noteArrayKey = cell.month + '_' + cell.date;
        const noteArray = notes[noteArrayKey] || [];

        const notesInDiv = noteArray.map(function (eachNote) {
          return <div className="Note"><span className="fa fa-circle"></span>{eachNote}</div>
        });
        
        return (
          <div className={className} onClick={onClick(cell.date, cell.month)}><div className="insideCell">
            <span>{cell.date}</span>
            <div className="Note-wrapper">
              {notesInDiv}
            </div>
          </div>
          </div>
        );
      })
      return (
        <div className = "Cell-row">
          {row}
        </div>
      );
    });
    
    return (
      <div>
        {allDates}
      </div>
    );
  }
}

class Sidebar extends React.Component {
  constructor(props) {
    super();
    this.state = {
      todoSubmitted: {},
      todoInput: '',
      month: props.month,
      editing: false,
      editInput: '',
      checked: false,
      todoTemp: [],
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.editClick = this.editClick.bind(this);
    this.doneClick = this.doneClick.bind(this);
    this.circleClick = this.circleClick.bind(this);
    this.editOnChange = this.editOnChange.bind(this);
    this.editOnSubmit = this.editOnSubmit.bind(this);
  }
  
  onChange(e) {
    this.setState({ todoInput: e.target.value });
  }

  onSubmit(e) {
    const { month, date } = this.props
    e.preventDefault();
    const { todoSubmitted } = this.state;
    const todoArrayKey = month + date;
    console.log(todoSubmitted);
    let todoArray = [];
    // ??? let instead of const?
    try {
      todoArray = [...todoSubmitted[todoArrayKey]];
    }
    catch(e) {
      todoArray = [];
    }
//    const todoArray = [...todoSubmitted[todoArrayKey]] || [];
    console.log(todoArray, 'todoArray')
    todoArray.push({todo: this.state.todoInput,   // Is this not a reference?
                    checked: false});
    this.setState({ todoSubmitted : {
      ...todoSubmitted,
      [todoArrayKey] : todoArray
      }
    });
    this.props.onSubmit(this.state.todoInput);
    this.setState({ todoInput: '' });

    request('POST', `http://localhost:5000/notes/${month}/${date}`, {
      notes: todoArray.map(todo => todo.todo),
    });
  }

  //When the user clicks edit and enters into edit mode.
  editClick(e) {
    this.setState({ editing: true });
  }

  //When the user clicks done in edit mode. 
  doneClick(key) {
    return (e) => {
      const { todoSubmitted, todoTemp } = this.state;
      //??? why is it necessary to put length here instead of just todoTemp?
      if (todoTemp.length) {
        this.setState({
          todoSubmitted: {
            ...todoSubmitted,
          [key]: [...todoTemp]
          }
        })
      }
      this.setState({ editing: false,
                      todoTemp: []});
    }
  }
  v       
  //When the user selects any of the todos in edit mode.
  circleClick(key, i) {
    return (e) => {
      const { todoSubmitted } = this.state;
      const notes = [...todoSubmitted[key]];
      notes[i] = {...notes[i], checked: true };
      this.setState({
        todoSubmitted: {
          ...todoSubmitted,
          [key]: notes,
        }
      });
    };
  }

  //When the user unselects any of the todos in edit mode.
  circleUnclick(key, i) {
    return (e) => {
      const { todoSubmitted } = this.state;
      const notes = [...todoSubmitted[key]];
      notes[i] = { ...notes[i], checked: false };
      this.setState({
	todoSubmitted: {
	  ...todoSubmitted,
	  [key]: notes,
        },
      });
    }
  }

  //When the user presses enter on any of the input in edit mode.
  editOnSubmit(key, index) {
    return (e) => {
      e.preventDefault();
      const { todoSubmitted } = this.state;
      const { editInput, todoTemp } = this.state;
      let temporaryNote = [];
      
      if(todoTemp.length == 0) {
        temporaryNote = [...todoSubmitted[key]];
      } else {
        temporaryNote = [...todoTemp];
      }

      //???? is there no problem referring to editInput here? 
      temporaryNote[index] = {todo: editInput,
                              checked: false}

      //todoTemp[index] = {todo: editInput,
      //checked: false};

      this.setState({ editInput: '',
                      todoTemp : temporaryNote });
    };
  }
  //When the user changes any of the input in edit mode.
  editOnChange(e) {
    this.setState({ editInput: e.target.value });
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.date !== nextProps.date || this.props.month !== nextProps.month) {
      this.setState({ editing: false });
      let { todoSubmitted } = this.state;
      const keys = Object.keys(todoSubmitted);
      for(let i=0; i < keys.length; i++) {
        const key = keys[i];
        todoSubmitted[key].map(
          (todo) => {todo.checked = false}
        );
      }
      this.setState({ todoSubmitted });
    }
  }

  render() {
    const {todoSubmitted, editing, checked} = this.state;
    const todoSubmittedKey = this.props.month + this.props.date;
    let editOrDone = <div></div>;
    if (editing) {
      editOrDone = <div className="pull-right">
        <span>Cancel</span>
        <div className="done" onClick={this.doneClick(todoSubmittedKey)}>Done</div>
      </div>;
    } else {
      editOrDone = <div className="fa fa-edit pull-right edit" onClick={this.editClick}></div>;
    }

    let todoInDiv = (todoSubmitted[todoSubmittedKey] || []).map((todo, i) => {
      if (editing && !todo.checked ) {
        return(
          <div className="Todo">
            <span className="fa fa-circle-o" onClick={this.circleClick(todoSubmittedKey, i)}></span>
            <form onSubmit={this.editOnSubmit(todoSubmittedKey, i)}>
              <input placeholder={todo.todo} onChange={this.editOnChange} />
            </form>
          </div>
        )
      } else if (editing && todo.checked) {
        return(
          <div className="Todo">
            <span className="fa fa-check-circle" onClick={this.circleUnclick(todoSubmittedKey, i)}></span>
            <form onSubmit={this.editOnSubmit(todoSubmittedKey, i)}>
              <input placeholder={todo.todo} onChange={this.editOnChange} />
            </form>
          </div>
        )
      } else {
        return(
          <div className="Todo" data-note={todo}>
            <span className="fa fa-circle"></span>
            {todo.todo}
          </div>
        )
      }
    });
    return (
      <div className="Sidebar">
        <span className="Sidebar-month">{this.props.month}</span>
        <h1>{this.props.date}</h1>
        {editOrDone}
        <div className="TodoWrapper">{todoInDiv}</div>
        <form onSubmit={this.onSubmit}>
          <input className="Entry" onChange={this.onChange} value={this.state.todoInput} placeholder="Type and press enter to save note" />
        </form>
      </div>
    );
  }
}

function Day() {
  const dayNames = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
  let Days = dayNames.map (
    (day) => { return (
      <div className="Day">
        {day}
      </div>
    )}
  );
  return (
    <div>
      {Days}
    </div>
  )
}

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      selectedDate: todayDate,
      selectedMonth: monthName[todayMonth],
      calendarMonth: todayMonth,
      previousMonth: todayMonth - 1,
      nextMonth: todayMonth + 1,
      calendarYear: todayYear,
      notes: {},
    };
  }

  cellOnClick(cellDate, cellMonth) {
    return (e) => {
      this.setState({ selectedDate: cellDate,
                      selectedMonth: cellMonth});
    };
  }

  leftClick(e) {
    const { previousMonth, nextMonth, calendarMonth, calendarYear } = this.state;
    if(calendarMonth  === 0) {
      this.setState({
        calendarMonth: 11,
        calendarYear: calendarYear - 1,
        previousMonth: 10,
        nextMonth: 0,
        selectedMonth: monthName[11],
        selectedDate: 1,
        note: '',
      });
    } else {
      this.setState({
        calendarMonth: calendarMonth - 1,
        previousMonth: calendarMonth - 2,
        nextMonth: calendarMonth,
        selectedMonth: monthName[calendarMonth - 1],
        selectedDate: 1,
        note: '',
        calendarYear: calendarYear,
      });
    }
  }

  rightClick(e) {
    const { previousMonth, nextMonth, calendarYear, calendarMonth } = this.state;
    if ( calendarMonth === 11) {
      this.setState({
        calendarMonth: 0,
        calendarYear: calendarYear + 1,
        previousMonth: 11,
        nextMonth: 1,
        selectedMonth: monthName[0],
        selectedDate: 1,
        note: '',
      });
    } else {
      this.setState({
        calendarMonth: calendarMonth + 1,
        previousMonth: calendarMonth,
        nextMonth: calendarMonth + 2,
        selectedMonth: monthName[calendarMonth + 1],
        selectedDate: 1,
      });
    }
  }

  noteOnSubmit(month, date) {
    return (todoInput) => {
      const { notes } = this.state;
      const oldNotes = notes[month + '_' + date] || [];
      oldNotes.push(todoInput);
      notes[month + '_' + date] = oldNotes;
      this.setState({ notes });
    };
  }

  editOnSubmit(month, date) {
    return (index, todo) => {
      const { notes } = this.state;
      const oldNotes = notes[month + '_' + date];
      oldNotes[index] = todo;
      notes[month + '_' + date] = oldNotes;
      this.setState({ notes });
    };
  }

  render() {
    const { selectedDate, selectedMonth, calendarMonth, calendarYear } = this.state;
    return (
      <div>
        <div className="Header">
          <div className="left-arrow" onClick={this.leftClick.bind(this)}><div className="fa fa-angle-left left"></div></div>
          <div className="Header-month"><h1>{monthName[this.state.calendarMonth]} {calendarYear}</h1></div>
          <div className="right-arrow" onClick={this.rightClick.bind(this)}>
            <div className="fa fa-angle-right right"></div></div>
        </div>
        <div className="Cell-row">
          <Day />
        </div>
        <Cell
          propYear={this.state.calendarYear}
          month={this.state.calendarMonth}
          onClick={this.cellOnClick.bind(this)}
          selectedDate={this.state.selectedDate}
          selectedMonth={this.state.selectedMonth}
          notes={this.state.notes}
        />
        <Sidebar date={this.state.selectedDate} month={this.state.selectedMonth} onSubmit={this.noteOnSubmit(this.state.selectedMonth, this.state.selectedDate)} editOnSubmit={this.editOnSubmit(this.state.selectedMonth, this.state.selectedDate)}/>
      </div>
    );
  }
}

module.exports = App;
